// AOS
AOS.init();

// FULLPAGE
$('#fullpage').fullpage({
    anchors: [
        'old','about','where','wines','images','others','footer'
    ],
    menu: '#menu',
    navigation: true,
});

// RELOAD
/* - Always Index (Don`t go to url slide [fullpage]) */
var url = window.location.href;
index = url.split("#").shift();
if(url != index){
    window.location.replace(index);
};
/* - Checkbox checked */
$(function(){
    var test = localStorage.input === 'true'? true: false;
    $('#yes-check, #legal-check').prop('checked', test || false);
});
$('#yes-check, #legal-check').on('change', function() {
    localStorage.input = $(this).is(':checked');
});

// MODAL - LEGAL
var modalLegal = new SimpleModal({
    title: '',
    body: '<p>¿QUÉ SON LAS COOKIES? <br><br> Una cookie es un fichero que se descarga en su ordenador (o el dispositivo que utilice: smartphone, tablet, etc…) al acceder a determinadas páginas web. Las cookies permiten a una página web, entre otras cosas, almacenar y recuperar información sobre los hábitos y preferencias de navegación de un usuario o de su equipo y, dependiendo de la información que contengan y de la forma en que utilice su equipo, pueden utilizarse para reconocer al usuario.<br><br>COOKIES UTILIZADAS EN ESTE SITIO WEB<br><br>Cookies de análisis. Son aquéllas que bien tratadas por nosotros o por terceros, nos permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del servicio ofertado. Para ello se analiza su navegación en nuestra página web con el fin de mejorar la oferta de productos o servicios que le ofrecemos. Para este cometido utilizamos las cookies del servicio de Google Analytics y Facebook.<br><br>Cookies técnicas. Son necesarias para la prestación de determinados servicios solicitados expresamente por el usuario como acceso a zonas privadas, información de sesión, etc. Sin estas cookies no se podrán ofrecer correctamente los servicios solicitados.<br><br>DESACTIVACIÓN O ELIMINACIÓN DE COOKIES<br><br>Usted puede permitir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones o preferencias del navegador utilizado para acceder a la web.<br><br>NOTAS ADICIONALES<br><br>Ni esta web ni sus representantes legales se hacen responsables ni del contenido ni de la veracidad de las políticas de privacidad que puedan tener los terceros mencionados en esta política de cookies.<br><br>Los navegadores web son las herramientas encargadas de almacenar las cookies y desde este lugar debe efectuar su derecho a eliminación o desactivación de las mismas. Ni esta web ni sus representantes legales pueden garantizar la correcta o incorrecta manipulación de las cookies por parte de los mencionados navegadores.<br><br>En algunos casos es necesario instalar cookies para que el navegador no olvide su decisión de no aceptación de las mismas.<br><br>En el caso de las cookies de Google Analytics, esta empresa almacena las cookies en servidores ubicados en Estados Unidos y se compromete a no compartirla con terceros, excepto en los casos en los que sea necesario para el funcionamiento del sistema o cuando la ley obligue a tal efecto. Según Google no guarda su dirección IP. Google Inc. es una compañía adherida al Acuerdo de Puerto Seguro que garantiza que todos los datos transferidos serán tratados con un nivel de protección acorde a la normativa europea.<br><br>Para cualquier duda o consulta acerca de esta política de cookies no dude en comunicarse con nosotros a través de la sección de contacto.</p>',
})
$( ".modalLegal" ).click(function() {
    modalLegal.show();
});

// SCROLL AGE
function disableScroll(){
    $.fn.fullpage.setMouseWheelScrolling(false);
    $.fn.fullpage.setAllowScrolling(false);
}
disableScroll();

function enableScroll(){
    $.fn.fullpage.setMouseWheelScrolling(true);
    $.fn.fullpage.setAllowScrolling(true);
    $(".nav-menu").removeClass("opacity0");
    $(".nav-menu").addClass("opacity1");
    setTimeout(function(){
        fullpage_api.moveTo('about', 0);
    }, 1600);      
}

function enableOrDisable(){
    let yesCheck = $("#yes-check").prop('checked');
    let noCheck = $("#no-check").prop('checked');
    let legalCheck = $("#legal-check").prop('checked');
    if(
        (yesCheck == true) &&
        (legalCheck == true)
    ){
        $(".confirm-age-yes").removeClass("disp-none");
        enableScroll();
    }else if(
        (noCheck == true) &&
        (legalCheck == true)
    ){
        $(".confirm-age-no").removeClass("disp-none");
        $(".nav-menu").removeClass("opacity1");
        $(".nav-menu").addClass("opacity0");
        disableScroll();
    };
}

function confirmAge(){
    if( !$(".confirm-age-yes").hasClass('disp-none') ){
        $(".confirm-age-yes").addClass("disp-none");
    }else if( !$(".confirm-age-no").hasClass('disp-none') ){
        $(".confirm-age-no").addClass("disp-none");
    }else{
        enableOrDisable()
    };
}

$("#submit-1, #submit-2").click(function(){
    confirmAge();
});

// MEDIUMZOOM
mediumZoom('.img-container img',{
    // Space outside the zoomed image
    margin: 0,
    // Color of the overlay
    background: "#181818",
    // Number of pixels to scroll to dismiss the zoom
    scrollOffset: 48,
    // Enables the action on meta click (opens the link / image source)
    metaClick: true,
    // Custom template
    template: ''
})

// CHANGE LANG
$(".change-lan").click(function(){
    $(".en").toggleClass('disp-block');
    $("label.en, span.en, p.en").toggleClass('disp-in-block');
    $(".es").toggleClass('disp-none');
});

// RRSS
$('.tlf').hover(function(){
    $('.display-tlf').toggleClass('disp-none');
});
$('.email').hover(function(){
    $('.display-email').toggleClass('disp-none');
});

// RIPLES
$('.about, .old, .where, .wines, .images, .others, .footer').ripples();
$('.about, .old, .where, .wines, .images, .others, .footer').ripples({
	resolution: 92,
	dropRadius: 16,
	perturbance: 0.04,
});

// CURSOR
const cursor = document.querySelector('.cursor');
document.addEventListener('mousemove', e => {
    cursor.setAttribute('style', 'top:' + (e.pageY - 10) + 'px; left:' + (e.pageX - 10) + 'px;');
});
